<?php
if (php_sapi_name() != 'cli') {
    echo 'This should be executed from console';
}
require_once __DIR__ . '/../../../vendor/autoload.php';

use Softko\Formatter\Console\ColorFormatter;

$formatter = new ColorFormatter();
echo $formatter->def('Default') . "\n";
echo $formatter->black('Black') . " (black)\n";
echo $formatter->red('Red') . "\n";
echo $formatter->green('Green') . "\n";
echo $formatter->yellow('Yellow') . "\n";
echo $formatter->blue('Blue') . "\n";
echo $formatter->magenta('Magenta') . "\n";
echo $formatter->cyan('Cyan') . "\n";
echo $formatter->gray('Gray') . "\n";
echo $formatter->grayDark('Gray dark') . "\n";
echo "\n";
$formatter->setColorType(ColorFormatter::COLOR_TYPE_NORMAL);
echo $formatter->def('Default color type is set to LIGHT. These are "normal" variants.') . "\n";
echo $formatter->red('Red normal') . "\n";
echo $formatter->green('Green normal') . "\n";
echo $formatter->yellow('Yellow normal') . "\n";
echo $formatter->blue('Blue normal') . "\n";
echo $formatter->magenta('Magenta normal') . "\n";
echo $formatter->cyan('Cyan normal') . "\n";
echo "\n";
$formatter->setColorType(ColorFormatter::COLOR_TYPE_LIGHT);
echo $formatter->def('Default bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->red('Red bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->green('Green bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->yellow('Yellow bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->blue('Blue bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->magenta('Magenta bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->cyan('Cyan bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->gray('Gray bold', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->grayDark('Gray dark bold', ColorFormatter::FONT_BOLD) . "\n";
echo "\n";
echo $formatter->def('Default dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->red('Red dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->green('Green dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->yellow('Yellow dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->blue('Blue dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->magenta('Magenta dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->cyan('Cyan dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->gray('Gray dark', ColorFormatter::FONT_DARK) . "\n";
echo $formatter->grayDark('Gray dark dark :)', ColorFormatter::FONT_DARK) . "\n";
echo "\n";
echo $formatter->def('Default underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->red('Red underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->green('Green underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->yellow('Yellow underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->blue('Blue underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->magenta('Magenta underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->cyan('Cyan underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->gray('Gray underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->grayDark('Gray dark underline', ColorFormatter::FONT_UNDERLINE) . "\n";
echo "\n";
echo $formatter->def('Default blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->red('Red blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->green('Green blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->yellow('Yellow blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->blue('Blue blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->magenta('Magenta blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->cyan('Cyan blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->gray('Gray blink', ColorFormatter::FONT_BLINK) . "\n";
echo $formatter->grayDark('Gray dark blink', ColorFormatter::FONT_BLINK) . "\n";
echo "\n";
echo $formatter->def('Default strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->red('Red strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->green('Green strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->yellow('Yellow strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->blue('Blue strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->magenta('Magenta strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->cyan('Cyan strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->gray('Gray strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->grayDark('Gray dark strike through', ColorFormatter::FONT_STRIKE) . "\n";
echo "\n";
echo $formatter->def('Default inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->red('Red inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->green('Green inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->yellow('Yellow inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->blue('Blue inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->magenta('Magenta inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->cyan('Cyan inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->gray('Gray inverted', ColorFormatter::FONT_INVERT) . "\n";
echo $formatter->grayDark('Gray dark inverted', ColorFormatter::FONT_INVERT) . "\n";
echo "\n";
echo $formatter->blackBackground('Black background') . "\n";
echo $formatter->redBackground('Red background') . "\n";
echo $formatter->greenBackground('Green background with bold text', ColorFormatter::FONT_BOLD) . "\n";
echo $formatter->yellowBackground('Yellow background with underline text', ColorFormatter::FONT_UNDERLINE) . "\n";
echo $formatter->blueBackground('Blue background with strike through text', ColorFormatter::FONT_STRIKE) . "\n";
echo $formatter->magentaBackground(
        'Magenta background with green text',
        ColorFormatter::FONT_NORMAL,
        ColorFormatter::COLOR_GREEN
    ) . "\n";
echo $formatter->cyanBackground(
        'Cyan background with red bold text',
        ColorFormatter::FONT_BOLD,
        ColorFormatter::COLOR_RED
    ) . "\n";
echo $formatter->grayBackground(
        'Gray background with blue text',
        ColorFormatter::FONT_NORMAL,
        ColorFormatter::COLOR_BLUE
    ) . "\n";
echo $formatter->grayDarkBackground(
        'Gray dark background with yellow text',
        ColorFormatter::FONT_NORMAL,
        ColorFormatter::COLOR_YELLOW
    ) . "\n";