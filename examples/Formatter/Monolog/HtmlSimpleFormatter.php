<?php
require_once __DIR__ . '/../../../vendor/autoload.php';

$handler = new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::DEBUG);
$handler->setFormatter(new \Softko\Formatter\Monolog\HtmlSimpleFormatter());
$logger = new \Monolog\Logger('example', [$handler]);

$logger->debug('This is debug message');
$logger->info('This is info message');
$logger->notice('This is notice message');
$logger->warning('This is warning message');
$logger->error('This is error message');
$logger->critical('This is critical message');
$logger->alert('This is alert message');
$logger->emergency('This is emergency message', ['context' => 'additional data']);