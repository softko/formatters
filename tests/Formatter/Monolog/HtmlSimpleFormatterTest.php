<?php
namespace Softko\Formatter\Monolog;

/**
 * @covers Softko\Formatter\Monolog\HtmlSimpleFormatter
 */
class HtmlSimpleFormatterTest extends \PHPUnit_Framework_TestCase
{
    const MSG_START = "<div class='monolog-log'>";
    const MSG_END = '</div>';
    /**
     * @var \DateTime
     */
    private $date;
    /**
     * @var ConsoleFormatter
     */
    private $formatter;

    private function getDate()
    {
        if (!$this->date) {
            $this->date = new \DateTime();
        }

        return $this->date;
    }

    protected function setUp()
    {
        $this->formatter = new HtmlSimpleFormatter(null, 'Y-m-d');
    }

    /**
     * @param array  $log
     * @param string $formatted
     *
     * @dataProvider formatDataProvider
     */
    public function testFormat(array $log, $formatted)
    {
        $this->assertEquals(
            self::MSG_START . $formatted . self::MSG_END,
            $this->formatter->format($log),
            $log['level_name'] . ' failed'
        );
    }

    /**
     * @param array  $log
     * @param string $formatted
     *
     * @dataProvider formatBatchDataProvider
     */
    public function testFormatBatch(array $log, $formatted)
    {
        $this->assertEquals($formatted, $this->formatter->formatBatch($log));
    }

    public function formatDataProvider()
    {
        $loggerStyle = 'style="color:orange"';

        return [
            [
                [
                    'level_name' => 'DEBUG',
                    'channel'    => 'log',
                    'context'    => [],
                    'message'    => 'foo',
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log</span>.<span style="color:gray;font-weight:bold">DEBUG</span>: foo  '
            ],
            [
                [
                    'level_name' => 'INFO',
                    'channel'    => 'log2',
                    'message'    => 'foo',
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                    'context'    => [
                        'foo'  => 'bar',
                        'baz'  => 'qux',
                        'bool' => false,
                        'null' => null,
                    ]
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log2</span>.<span style="color:green;font-weight:bold">INFO</span>: foo ' . '{"foo":"bar","baz":"qux","bool":false,"null":null} '
            ],
            [
                [
                    'level_name' => 'NOTICE',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['ip' => '127.0.0.1', 'referer' => 'test'],
                    'message'    => 'bar',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log</span>.<span style="color:cyan;font-weight:bold">NOTICE</span>: bar  ' . '{"ip":"127.0.0.1","referer":"test"}'
            ],
            [
                [
                    'level_name' => 'WARNING',
                    'channel'    => 'logger',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['session' => 'abc', 'user' => 'admin'],
                    'message'    => 'bar',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>logger</span>.<span style="color:magenta;font-weight:bold">WARNING</span>: bar  ' . '{"session":"abc","user":"admin"}'
            ],
            [
                [
                    'level_name' => 'ERROR',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => [
                        'foo' => new TestHtmlSimpleFoo,
                        'bar' => new TestHtmlSimpleBar,
                        'baz' => [],
                        'res' => fopen('php://memory', 'rb')
                    ],
                    'message'    => 'lipsum',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log</span>.<span style="color:red;font-weight:bold">ERROR</span>: lipsum  ' . '{"foo":"[object] (Softko\\\\Formatter\\\\Monolog\\\\TestHtmlSimpleFoo: {\"foo\":\"foo\"})","bar":"[object] (Softko\\\\Formatter\\\\Monolog\\\\TestHtmlSimpleBar: {})","baz":[],"res":"[resource]"}'
            ],
            [
                [
                    'level_name' => 'CRITICAL',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                    'message'    => 'crit',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log</span>.<span style="color:red;font-weight:bold">CRITICAL</span>: crit  '
            ],
            [
                [
                    'level_name' => 'ALERT',
                    'channel'    => 'app',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['terminate' => true],
                    'message'    => 'Something is really going doooooown',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>app</span>.<span style="background-color:red;color:white">ALERT</span>: Something is really going doooooown  ' . '{"terminate":true}'
            ],
            [
                [
                    'level_name' => 'EMERGENCY',
                    'channel'    => 'app',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['action' => 'blow'],
                    'message'    => 'you\'re doomed',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>app</span>.<span style="background-color:red;color:white;font-weight:bold">EMERGENCY</span>: you\'re doomed  ' . '{"action":"blow"}'
            ],
        ];
    }

    public function formatBatchDataProvider()
    {
        $loggerStyle = 'style="color:orange"';

        return [
            [
                [
                    [
                        'level_name' => 'DEBUG',
                        'channel'    => 'log',
                        'message'    => 'foo',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ],
                    [
                        'level_name' => 'ALERT',
                        'channel'    => 'log2',
                        'message'    => 'bar',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ],
                    [
                        'level_name' => 'EMERGENCY',
                        'channel'    => 'log3',
                        'message'    => 'qux',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ]
                ],
                self::MSG_START . '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log</span>.<span style="color:gray;font-weight:bold">DEBUG</span>: foo  ' . self::MSG_END .
                self::MSG_START . '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log2</span>.<span style="background-color:red;color:white">ALERT</span>: bar  ' . self::MSG_END .
                self::MSG_START . '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . '] <span ' . $loggerStyle . '>log3</span>.<span style="background-color:red;color:white;font-weight:bold">EMERGENCY</span>: qux  ' . self::MSG_END
            ]
        ];
    }
}

class TestHtmlSimpleFoo
{
    public $foo = 'foo';
}

class TestHtmlSimpleBar
{
    public function __toString()
    {
        return 'bar';
    }
}