<?php
namespace Softko\Formatter\Monolog;

/**
 * @covers Softko\Formatter\Monolog\ConsoleFormatter
 */
class ConsoleFormatterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \DateTime
     */
    private $date;
    /**
     * @var ConsoleFormatter
     */
    private $formatter;

    private function getDate()
    {
        if (!$this->date) {
            $this->date = new \DateTime();
        }

        return $this->date;
    }

    protected function setUp()
    {
        $this->formatter = new ConsoleFormatter(null, 'Y-m-d');
    }

    /**
     * @param array  $log
     * @param string $formatted
     *
     * @dataProvider formatDataProvider
     */
    public function testFormat(array $log, $formatted)
    {
        $this->assertEquals($formatted, $this->formatter->format($log), $log['level_name'] . ' failed');
    }

    /**
     * @param array  $log
     * @param string $formatted
     *
     * @dataProvider formatBatchDataProvider
     */
    public function testFormatBatch(array $log, $formatted)
    {
        $this->assertEquals($formatted, $this->formatter->formatBatch($log));
    }

    public function formatDataProvider()
    {
        return [
            [
                [
                    'level_name' => 'DEBUG',
                    'channel'    => 'log',
                    'context'    => [],
                    'message'    => 'foo',
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                ],
                '[' . $this->getDate()->format('Y-m-d') . "] \033[1;93mlog\033[0m.\033[1;37mDEBUG\033[0m: foo  \n"
            ],
            [
                [
                    'level_name' => 'INFO',
                    'channel'    => 'log2',
                    'message'    => 'foo',
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                    'context'    => [
                        'foo'  => 'bar',
                        'baz'  => 'qux',
                        'bool' => false,
                        'null' => null,
                    ]
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mlog2\033[0m.\033[1;92mINFO\033[0m: foo " . '{"foo":"bar","baz":"qux","bool":false,"null":null} ' . "\n"
            ],
            [
                [
                    'level_name' => 'NOTICE',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['ip' => '127.0.0.1', 'referer' => 'test'],
                    'message'    => 'bar',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mlog\033[0m.\033[1;96mNOTICE\033[0m: bar  " . '{"ip":"127.0.0.1","referer":"test"}' . "\n"
            ],
            [
                [
                    'level_name' => 'WARNING',
                    'channel'    => 'logger',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['session' => 'abc', 'user' => 'admin'],
                    'message'    => 'bar',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mlogger\033[0m.\033[1;95mWARNING\033[0m: bar  " . '{"session":"abc","user":"admin"}' . "\n"
            ],
            [
                [
                    'level_name' => 'ERROR',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => [
                        'foo' => new TestConsoleFoo,
                        'bar' => new TestConsoleBar,
                        'baz' => [],
                        'res' => fopen('php://memory', 'rb')
                    ],
                    'message'    => 'lipsum',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mlog\033[0m.\033[1;91mERROR\033[0m: lipsum  " . '{"foo":"[object] (Softko\\\\Formatter\\\\Monolog\\\\TestConsoleFoo: {\"foo\":\"foo\"})","bar":"[object] (Softko\\\\Formatter\\\\Monolog\\\\TestConsoleBar: {})","baz":[],"res":"[resource]"}' . "\n"
            ],
            [
                [
                    'level_name' => 'CRITICAL',
                    'channel'    => 'log',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => [],
                    'message'    => 'crit',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mlog\033[0m.\033[1;91mCRITICAL\033[0m: crit  " . "\n"
            ],
            [
                [
                    'level_name' => 'ALERT',
                    'channel'    => 'app',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['terminate' => true],
                    'message'    => 'Something is really going doooooown',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mapp\033[0m.\033[0;41mALERT\033[0m: Something is really going doooooown  " . '{"terminate":true}' . "\n"
            ],
            [
                [
                    'level_name' => 'EMERGENCY',
                    'channel'    => 'app',
                    'context'    => [],
                    'datetime'   => $this->getDate(),
                    'extra'      => ['action' => 'blow'],
                    'message'    => 'you\'re doomed',
                ],
                '[' . $this->getDate()->format(
                    'Y-m-d'
                ) . "] \033[1;93mapp\033[0m.\033[1;41mEMERGENCY\033[0m: you're doomed  " . '{"action":"blow"}' . "\n"
            ],
        ];
    }

    public function formatBatchDataProvider()
    {
        return [
            [
                [
                    [
                        'level_name' => 'DEBUG',
                        'channel'    => 'log',
                        'message'    => 'foo',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ],
                    [
                        'level_name' => 'ALERT',
                        'channel'    => 'log2',
                        'message'    => 'bar',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ],
                    [
                        'level_name' => 'EMERGENCY',
                        'channel'    => 'log3',
                        'message'    => 'qux',
                        'context'    => [],
                        'datetime'   => $this->getDate(),
                        'extra'      => [],
                    ]
                ],
                '[' . $this->getDate()->format('Y-m-d') . "] \033[1;93mlog\033[0m.\033[1;37mDEBUG\033[0m: foo  \n" .
                '[' . $this->getDate()->format('Y-m-d') . "] \033[1;93mlog2\033[0m.\033[0;41mALERT\033[0m: bar  \n" .
                '[' . $this->getDate()->format('Y-m-d') . "] \033[1;93mlog3\033[0m.\033[1;41mEMERGENCY\033[0m: qux  \n"
            ]
        ];
    }
}

class TestConsoleFoo
{
    public $foo = 'foo';
}

class TestConsoleBar
{
    public function __toString()
    {
        return 'bar';
    }
}