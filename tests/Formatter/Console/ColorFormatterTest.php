<?php
namespace Softko\Formatter\Console;

/**
 * @covers Softko\Formatter\Console\ColorFormatter
 */
class ColorFormatterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ColorFormatter
     */
    private $formatter;

    protected function setUp()
    {
        $this->formatter = new ColorFormatter();
    }

    public function testDefaultColorType()
    {
        $this->assertSame(
            "\033[0;91mdefault\033[0m",
            $this->formatter->red('default'),
            'Wrong default color type failed'
        );
    }

    /**
     * @param string $method    Method name to test
     * @param string $normal    Expected value for normal font
     * @param string $bold      Expected value for bold font
     * @param string $dark      Expected value for dark font
     * @param string $underline Expected value for underline font
     * @param string $blink     Expected value for blink font
     * @param string $inverted  Expected value for inverted font
     * @param string $strike    Expected value for strike through font
     *
     * @dataProvider simpleColorDataProvider
     */
    public function testSimpleColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike)
    {
        $this->formatter->setColorType(ColorFormatter::COLOR_TYPE_NORMAL);
        $this->simpleColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike);
    }

    /**
     * @param string $method    Method name to test
     * @param string $normal    Expected value for normal font
     * @param string $bold      Expected value for bold font
     * @param string $dark      Expected value for dark font
     * @param string $underline Expected value for underline font
     * @param string $blink     Expected value for blink font
     * @param string $inverted  Expected value for inverted font
     * @param string $strike    Expected value for strike through font
     *
     * @dataProvider simpleLightColorDataProvider
     */
    public function testSimpleLightColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike)
    {
        $this->formatter->setColorType(ColorFormatter::COLOR_TYPE_LIGHT);
        $this->simpleColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike);
    }

    /**
     * @param string $method    Method name to test
     * @param string $normal    Expected value for normal font
     * @param string $bold      Expected value for bold font
     * @param string $dark      Expected value for dark font
     * @param string $underline Expected value for underline font
     * @param string $blink     Expected value for blink font
     * @param string $inverted  Expected value for inverted font
     * @param string $strike    Expected value for strike through font
     *
     * @dataProvider simpleBackgroundColorDataProvider
     */
    public function testSimpleBackgroundColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike)
    {
        $this->simpleColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike);
    }

    /**
     * @param string $method Method name to test
     * @param string $fontType
     * @param string $foreColor
     * @param string $expectedValue
     *
     * @dataProvider advancedBackgroundColorDataProvider
     */
    public function testAdvancedBackgroundColor($method, $fontType, $foreColor, $expectedValue)
    {
        $this->assertSame(
            sprintf($expectedValue, 'formatted text'),
            $this->formatter->{$method}('formatted text', $fontType, $foreColor),
            $method . ', ' . $fontType . ', ' . $foreColor . ' failed'
        );
    }

    private function simpleColor($method, $normal, $bold, $dark, $underline, $blink, $inverted, $strike)
    {
        $this->assertSame(
            sprintf($normal, 'normal text'),
            $this->formatter->{$method}('normal text'),
            $method . ' normal failed'
        );
        $this->assertSame(
            sprintf($bold, 'bold text'),
            $this->formatter->{$method}('bold text', ColorFormatter::FONT_BOLD),
            $method . ' bold failed'
        );
        $this->assertSame(
            sprintf($dark, 'dark text'),
            $this->formatter->{$method}('dark text', ColorFormatter::FONT_DARK),
            $method . ' dark failed'
        );
        $this->assertSame(
            sprintf($underline, 'underline text'),
            $this->formatter->{$method}('underline text', ColorFormatter::FONT_UNDERLINE),
            $method . ' underline failed'
        );
        $this->assertSame(
            sprintf($blink, 'blink text'),
            $this->formatter->{$method}('blink text', ColorFormatter::FONT_BLINK),
            $method . ' blink failed'
        );
        $this->assertSame(
            sprintf($inverted, 'inverted text'),
            $this->formatter->{$method}('inverted text', ColorFormatter::FONT_INVERT),
            $method . ' invert failed'
        );
        $this->assertSame(
            sprintf($strike, 'strike through text'),
            $this->formatter->{$method}('strike through text', ColorFormatter::FONT_STRIKE),
            $method . ' strike failed'
        );
    }

    public function simpleColorDataProvider()
    {
        $colors = [
            'black'    => 30,
            'red'      => 31,
            'green'    => 32,
            'yellow'   => 33,
            'blue'     => 34,
            'magenta'  => 35,
            'cyan'     => 36,
            'gray'     => 37,
            'grayDark' => 90,
        ];

        return $this->getData($colors);
    }

    public function simpleLightColorDataProvider()
    {
        $colors = [
            'black'    => 30,
            'red'      => 91,
            'green'    => 92,
            'yellow'   => 93,
            'blue'     => 94,
            'magenta'  => 95,
            'cyan'     => 96,
            'gray'     => 37,
            'grayDark' => 90,
        ];

        return $this->getData($colors);
    }

    public function simpleBackgroundColorDataProvider()
    {
        $colors = [
            'black'    => 40,
            'red'      => 41,
            'green'    => 42,
            'yellow'   => 43,
            'blue'     => 44,
            'magenta'  => 45,
            'cyan'     => 46,
            'gray'     => 47,
            'grayDark' => 100,
        ];

        return $this->getData($colors, false, 'Background');
    }

    public function advancedBackgroundColorDataProvider()
    {
        $s = "\033[";
        $e = "m%s\033[0m";

        return [
            [
                'magentaBackground',
                ColorFormatter::FONT_BOLD,
                ColorFormatter::COLOR_GREEN,
                $s . "1;92;45" . $e
            ],
            [
                'blueBackground',
                ColorFormatter::FONT_UNDERLINE,
                ColorFormatter::COLOR_YELLOW,
                $s . "4;93;44" . $e
            ],
            [
                'yellowBackground',
                ColorFormatter::FONT_STRIKE,
                ColorFormatter::COLOR_RED,
                $s . "9;91;43" . $e
            ],
            [
                'grayDarkBackground',
                ColorFormatter::FONT_NORMAL,
                ColorFormatter::COLOR_MAGENTA,
                $s . "0;95;100" . $e
            ],
            [
                'cyanBackground',
                ColorFormatter::FONT_BLINK,
                ColorFormatter::COLOR_GRAY,
                $s . "5;37;46" . $e
            ],
        ];
    }

    private function getData($colors, $default = true, $methodSuffix = '')
    {
        $data = [];
        $s = "\033[";
        $e = "m%s\033[0m";
        if ($default) {
            $data = [
                [
                    'def',
                    $s . '0' . $e,
                    $s . '1' . $e,
                    $s . '2' . $e,
                    $s . '4' . $e,
                    $s . '5' . $e,
                    $s . '7' . $e,
                    $s . '9' . $e,
                ]
            ];
        }
        foreach ($colors as $name => $colorCode) {
            $data[] = [
                $name . $methodSuffix,
                $s . "0;" . $colorCode . $e,
                $s . "1;" . $colorCode . $e,
                $s . "2;" . $colorCode . $e,
                $s . "4;" . $colorCode . $e,
                $s . "5;" . $colorCode . $e,
                $s . "7;" . $colorCode . $e,
                $s . "9;" . $colorCode . $e
            ];
        }

        return $data;
    }
}