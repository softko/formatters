<?php

namespace Softko\Formatter\Monolog;

use Monolog\Formatter\NormalizerFormatter;
use Softko\Formatter\Console\ColorFormatter;

/**
 * ConsoleFormatter for Monolog.
 * Formats incoming records by adding colors to console output.
 */
class ConsoleFormatter extends NormalizerFormatter
{
    const SIMPLE_FORMAT = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";

    private $format;
    /**
     * @var ColorFormatter
     */
    private $formatter;

    public function __construct($format = null, $dateFormat = null, $colorType = ColorFormatter::COLOR_TYPE_LIGHT)
    {
        $this->format = $format ?: static::SIMPLE_FORMAT;
        $this->formatter = new ColorFormatter();
        $this->formatter->setColorType($colorType);
        parent::__construct($dateFormat);
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $vars = parent::format($record);
        $output = $this->format;

        foreach ($vars['extra'] as $var => $val) {
            if (false !== strpos($output, '%extra.' . $var . '%')) {
                $output = str_replace('%extra.' . $var . '%', $this->convertToString($val), $output);
                unset($vars['extra'][$var]);
            }
        }

        if (empty($vars['context'])) {
            unset($vars['context']);
            $output = str_replace('%context%', '', $output);
        }

        if (empty($vars['extra'])) {
            unset($vars['extra']);
            $output = str_replace('%extra%', '', $output);
        }

        // colorize
        $vars['level_name'] = $this->formatByLevelName($vars['level_name']);
        $vars['channel'] = $this->formatter->yellow($vars['channel'], ColorFormatter::FONT_BOLD);

        foreach ($vars as $var => $val) {
            if (false !== strpos($output, '%' . $var . '%')) {
                $output = str_replace('%' . $var . '%', $this->convertToString($val), $output);
            }
        }

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function formatBatch(array $records)
    {
        foreach ($records as $key => $record) {
            $records[$key] = $this->format($record);
        }

        return implode('', $records);
    }

    private function convertToString($data)
    {
        if (null === $data || is_bool($data)) {
            return var_export($data, true);
        }

        if (is_scalar($data)) {
            return (string)$data;
        }

        return $this->toJson($data, true);
    }

    private function formatByLevelName($levelName)
    {
        switch ($levelName) {
            case 'DEBUG':
                return $this->formatter->gray($levelName, ColorFormatter::FONT_BOLD);
            case 'INFO':
                return $this->formatter->green($levelName, ColorFormatter::FONT_BOLD);
            case 'NOTICE':
                return $this->formatter->cyan($levelName, ColorFormatter::FONT_BOLD);
            case 'WARNING':
                return $this->formatter->magenta($levelName, ColorFormatter::FONT_BOLD);
            case 'ERROR':
                return $this->formatter->red($levelName, ColorFormatter::FONT_BOLD);
            case 'CRITICAL':
                return $this->formatter->red($levelName, ColorFormatter::FONT_BOLD);
            case 'ALERT':
                return $this->formatter->redBackground($levelName);
            case 'EMERGENCY':
                return $this->formatter->redBackground($levelName, ColorFormatter::FONT_BOLD);
            default:
                return $levelName;
        }
    }
}
