<?php

namespace Softko\Formatter\Monolog;

use Monolog\Formatter\NormalizerFormatter;

/**
 * HtmlSimpleFormatter for Monolog.
 * Formats incoming records by adding colors to the logger and level.
 * Whole message is surrounded with <div> element with "monolog-log" css class
 */
class HtmlSimpleFormatter extends NormalizerFormatter
{
    const SIMPLE_FORMAT = "<div class='monolog-log'>[%datetime%] %channel%.%level_name%: %message% %context% %extra%</div>";

    private $format;

    public function __construct($format = null, $dateFormat = null)
    {
        $this->format = $format ?: static::SIMPLE_FORMAT;
        parent::__construct($dateFormat);
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $vars = parent::format($record);
        $output = $this->format;

        foreach ($vars['extra'] as $var => $val) {
            if (false !== strpos($output, '%extra.' . $var . '%')) {
                $output = str_replace('%extra.' . $var . '%', $this->convertToString($val), $output);
                unset($vars['extra'][$var]);
            }
        }

        if (empty($vars['context'])) {
            unset($vars['context']);
            $output = str_replace('%context%', '', $output);
        }

        if (empty($vars['extra'])) {
            unset($vars['extra']);
            $output = str_replace('%extra%', '', $output);
        }

        // colorize
        $vars['level_name'] = $this->formatByLevelName($vars['level_name']);
        $vars['channel'] = $this->colorizeText($vars['channel'], 'orange');

        foreach ($vars as $var => $val) {
            if (false !== strpos($output, '%' . $var . '%')) {
                $output = str_replace('%' . $var . '%', $this->convertToString($val), $output);
            }
        }

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function formatBatch(array $records)
    {
        foreach ($records as $key => $record) {
            $records[$key] = $this->format($record);
        }

        return implode('', $records);
    }

    private function convertToString($data)
    {
        if (null === $data || is_bool($data)) {
            return var_export($data, true);
        }

        if (is_scalar($data)) {
            return (string)$data;
        }

        return $this->toJson($data, true);
    }

    private function formatByLevelName($levelName)
    {
        switch ($levelName) {
            case 'DEBUG':
                return $this->colorizeText($levelName, 'gray', true);
            case 'INFO':
                return $this->colorizeText($levelName, 'green', true);
            case 'NOTICE':
                return $this->colorizeText($levelName, 'cyan', true);
            case 'WARNING':
                return $this->colorizeText($levelName, 'magenta', true);
            case 'ERROR':
                return $this->colorizeText($levelName, 'red', true);
            case 'CRITICAL':
                return $this->colorizeText($levelName, 'red', true);
            case 'ALERT':
                return $this->colorizeText($levelName, 'white', false, true, 'red');
            case 'EMERGENCY':
                return $this->colorizeText($levelName, 'white', true, true, 'red');
            default:
                return $levelName;
        }
    }

    private function colorizeText($text, $color, $bold = false, $bg = false, $bgColor = 'white')
    {
        return '<span style="' . ($bg ? 'background-color:' . $bgColor . ';' : '') . 'color:' . $color . '' . ($bold ? ';font-weight:bold' : '') . '">' . $text . '</span>';
    }
}
