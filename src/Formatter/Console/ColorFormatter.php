<?php

namespace Softko\Formatter\Console;

/**
 * Console\ColorFormatter adds colors to console output.
 * It can format fore and background color, make text bold, dark, underline, blink, invert or strike through.
 *
 * Don't forget that different terminals have different text format compatibility.
 */
class ColorFormatter
{
    const COLOR_BLACK = 'black';
    const COLOR_RED = 'red';
    const COLOR_GREEN = 'green';
    const COLOR_BLUE = 'blue';
    const COLOR_YELLOW = 'yellow';
    const COLOR_MAGENTA = 'magenta';
    const COLOR_CYAN = 'cyan';
    const COLOR_GRAY = 'gray';
    const COLOR_GRAY_DARK = 'gray_dark';

    const FONT_NORMAL = '0';
    const FONT_BOLD = '1';
    const FONT_DARK = '2';
    const FONT_UNDERLINE = '4';
    const FONT_BLINK = '5';
    const FONT_INVERT = '7';
    const FONT_STRIKE = '9';

    const COLOR_TYPE_NORMAL = 0;
    const COLOR_TYPE_LIGHT = 1;

    /**
     * @var array [light, normal, background]
     */
    private $color = [
        self::COLOR_BLACK     => ['30', '30', '40'],
        self::COLOR_RED       => ['31', '91', '41'],
        self::COLOR_GREEN     => ['32', '92', '42'],
        self::COLOR_YELLOW    => ['33', '93', '43'],
        self::COLOR_BLUE      => ['34', '94', '44'],
        self::COLOR_MAGENTA   => ['35', '95', '45'],
        self::COLOR_CYAN      => ['36', '96', '46'],
        self::COLOR_GRAY      => ['37', '37', '47'],
        self::COLOR_GRAY_DARK => ['90', '90', '100'],
    ];

    private $colorType = self::COLOR_TYPE_LIGHT;

    /**
     * Sets lighter/darker colors
     *
     * @param string $type See COLOR_TYPE_* constants for available color types
     */
    public function setColorType($type)
    {
        if (in_array($type, [self::COLOR_TYPE_NORMAL, self::COLOR_TYPE_LIGHT])) {
            $this->colorType = $type;
        }
    }

    /**
     * Changes text color to default terminal color
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function def($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, '', $fontType);
    }

    /**
     * Changes text color to black
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function black($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_BLACK, $fontType);
    }

    /**
     * Changes text color to red
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function red($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_RED, $fontType);
    }

    /**
     * Changes text color to green
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function green($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_GREEN, $fontType);
    }

    /**
     * Changes text color to yellow
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function yellow($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_YELLOW, $fontType);
    }

    /**
     * Changes text color to blue
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function blue($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_BLUE, $fontType);
    }

    /**
     * Changes text color to magenta
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function magenta($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_MAGENTA, $fontType);
    }

    /**
     * Changes text color to cyan
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function cyan($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_CYAN, $fontType);
    }

    /**
     * Changes text color to gray
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function gray($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_GRAY, $fontType);
    }

    /**
     * Changes text color to dark gray
     *
     * @param string $text     Text to colorize
     * @param string $fontType See FONT_* constants for available font types
     *
     * @return string Colorized string
     */
    public function grayDark($text, $fontType = self::FONT_NORMAL)
    {
        return $this->colorize($text, self::COLOR_GRAY_DARK, $fontType);
    }

    /**
     * Changes text background color to black, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function blackBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_BLACK);
    }

    /**
     * Changes text background color to red, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function redBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_RED);
    }

    /**
     * Changes text background color to green, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function greenBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_GREEN);
    }

    /**
     * Changes text background color to yellow, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function yellowBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_YELLOW);
    }

    /**
     * Changes text background color to blue, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function blueBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_BLUE);
    }

    /**
     * Changes text background color to magenta, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function magentaBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_MAGENTA);
    }

    /**
     * Changes text background color to cyan, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function cyanBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_CYAN);
    }

    /**
     * Changes text background color to gray, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function grayBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_GRAY);
    }

    /**
     * Changes text background color to dark gray, optionally changes text's fore color
     *
     * @param string $text      Text to colorize
     * @param string $fontType  See FONT_* constants for available font types
     * @param string $foreColor See COLOR_* constants for available font fore colors
     *
     * @return string Colorized string
     */
    public function grayDarkBackground($text, $fontType = self::FONT_NORMAL, $foreColor = '')
    {
        return $this->colorizeWithBackground($text, $foreColor, $fontType, self::COLOR_GRAY_DARK);
    }

    private function colorize($text, $foreColor, $fontType = self::FONT_NORMAL)
    {
        return "\033[" . $this->getColorCode($foreColor, $fontType) . "m" . $text . "\033[0m";
    }

    private function colorizeWithBackground($text, $foreColor, $fontType, $bgColor)
    {
        $bg = isset($this->color[$bgColor]) ? $this->color[$bgColor][2] : '';

        return "\033[" . $this->getColorCode($foreColor, $fontType) . ";" . $bg . "m" . $text . "\033[0m";
    }

    private function getColorCode($color, $fontType)
    {
        return isset($this->color[$color]) ? $fontType . ';' . $this->color[$color][$this->colorType] : $fontType;
    }
}
