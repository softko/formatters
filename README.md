#Formatters
***
##Installation
You can install the library using composer by adding `softko/formatters` to *require* section and `https://softko@bitbucket.org/softko/formatters.git` to *repositories* section (type: vcs) of your `composer.json`.

##Current formatters

* console
	* color
* monolog [monolog](https://github.com/Seldaek/monolog)
	* console
	* htmlSimple